package net.orandja.ucm;

import net.fabricmc.api.ModInitializer;
import net.minecraft.util.registry.BuiltinRegistries;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeKeys;
import net.minecraft.world.biome.source.BiomeLayerSampler;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class UCM implements ModInitializer {

    public static BiomeLayerSampler biomeSampler = null;

    private final Field depthField = getField(Biome.class, "depth", "field_9343", "h");
    private final Field scaleField = getField(Biome.class, "scale", "field_9341", "i");

    private void setValue(RegistryKey<Biome> biomeKey, float mod, float mod2) {
        try {
            Biome biome = BuiltinRegistries.BIOME.get(biomeKey.getValue());
            depthField.set(biome, ((float) depthField.get(biome)) * mod);
            scaleField.set(biome, ((float) scaleField.get(biome)) * mod2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ///execute in minecraft:overworld run tp @s -1218.71 66.59 1602.82 59.55 63.45

    private Field getField(Class clazz, String... fieldNames) {
        for (String fieldName : fieldNames) {
            Field field = null;
//            try {
//                field = clazz.getDeclaredField(fieldName);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            for (Field declaredField : clazz.getDeclaredFields()) {
                if (declaredField.getName().equalsIgnoreCase(fieldName)) {
                    field = declaredField;
                    break;
                }
            }

            if (field != null) {
                try {
                    field.setAccessible(true);
                    Field modifiersField = Field.class.getDeclaredField("modifiers");
                    modifiersField.setAccessible(true);
                    modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
                    return field;
                } catch (Exception ignored) {
                    try {
                        int mods = field.getModifiers();
                        if (Modifier.isFinal(mods)) {
                            java.lang.invoke.VarHandle MODIFIERS = java.lang.invoke.MethodHandles.privateLookupIn(Field.class, java.lang.invoke.MethodHandles.lookup()).findVarHandle(Field.class, "modifiers", int.class);
                            MODIFIERS.set(field, mods & ~Modifier.FINAL);
                        }

                        return field;
                    } catch (Exception ignored2) {
                        ignored.printStackTrace();
                        ignored2.printStackTrace();
                    }
                }
            }
        }
//
//        System.out.println("Declared Fields");
//
//        System.out.println("Fields");
//
//        for (Field declaredField : clazz.getFields()) {
//            System.out.println(declaredField.getType() + "  " + declaredField.getName());
//        }
        return null;
    }

    @Override
    public void onInitialize() {
        setValue(BiomeKeys.MOUNTAINS, 2.5f, 2.5f);
        setValue(BiomeKeys.SNOWY_MOUNTAINS, 2.5f, 2.5f);
        setValue(BiomeKeys.GRAVELLY_MOUNTAINS, 2.5f, 2.5f);
        setValue(BiomeKeys.MODIFIED_GRAVELLY_MOUNTAINS, 2.5f, 2.5f);
        setValue(BiomeKeys.TAIGA_MOUNTAINS, 2.5f, 2.5f);

        setValue(BiomeKeys.MOUNTAIN_EDGE, 1.5f, 1.5f);

        // PLATEAU
        setValue(BiomeKeys.SAVANNA_PLATEAU, 1.2f, 1f);
        setValue(BiomeKeys.WOODED_BADLANDS_PLATEAU, 1.2f, 1f);
        setValue(BiomeKeys.MODIFIED_WOODED_BADLANDS_PLATEAU, 1.2f, 1f);
        setValue(BiomeKeys.BADLANDS_PLATEAU, 1.2f, 1f);
        setValue(BiomeKeys.MODIFIED_BADLANDS_PLATEAU, 1.2f, 1f);
        setValue(BiomeKeys.ERODED_BADLANDS, 1.2f, 1f);

        // HILLS
        setValue(BiomeKeys.DESERT_HILLS, 1.1f, 1.1f);
        setValue(BiomeKeys.WOODED_HILLS, 1.5f, 1.5f);
        setValue(BiomeKeys.TAIGA_HILLS, 1.5f, 1.5f);
        setValue(BiomeKeys.JUNGLE_HILLS, 1.5f, 1.5f);
        setValue(BiomeKeys.BIRCH_FOREST_HILLS, 1.5f, 1.5f);
        setValue(BiomeKeys.SNOWY_TAIGA_HILLS, 1.5f, 1.5f);
        setValue(BiomeKeys.GIANT_TREE_TAIGA_HILLS, 1.5f, 1.5f);
        setValue(BiomeKeys.SWAMP_HILLS, 1.5f, 1.5f);
        setValue(BiomeKeys.TALL_BIRCH_HILLS, 1.5f, 1.5f);
        setValue(BiomeKeys.DARK_FOREST_HILLS, 1.5f, 1.5f);
        setValue(BiomeKeys.DARK_FOREST, 1.75f, 1.0f);
        setValue(BiomeKeys.GIANT_SPRUCE_TAIGA_HILLS, 1.5f, 1.5f);
        setValue(BiomeKeys.BAMBOO_JUNGLE_HILLS, 1.5f, 1.5f);
    }
}
